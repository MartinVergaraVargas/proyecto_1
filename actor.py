#!/usr/bin/env python3
# -*- coding:utf-8 -*-



class Actor():

    def __init__(self):
        self.__nombre = None
        self.__peliculas = []

    def get_nombre(self):
        return self.__nombre

    def set_nombre(self, nombre):
        if isinstance(nombre, str):
            self.__nombre = nombre

    def get_peliculas(self):
        return self.__peliculas

    def set_peliculas(self, pelicula):
        self.__peliculas.append(pelicula)
