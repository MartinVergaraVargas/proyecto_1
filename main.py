#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import pandas as pd
from actor import Actor
from director import Director
from pelicula import Pelicula


import os
def clear_screen(): 
    os.system('cls' if os.name=='nt' else 'clear') #con esta funcion se limpia la terminal

#os.system('mode con: cols=400 lines=400')

def procesa_archivo():
    data = pd.read_csv("jetmovies.csv", sep = ";") #funcion para leer el archivo.csv que se crea del filtrado del archivo entregado en clases
    print(data.columns)                            #parte innecesaria de la funcion, pero que mantuve porque se ve linda cuando corre el codigo
    return data

def filtra_columnas(data):                         #funcion que crea un archivo.csv filtrando solo las columnas que seran usadas en este proyecto
    jetmovies1 = data.filter(["title", "year", "genre", "duration", "country", "language", "director", "avg_vote", "actors"])
    jetmovies = jetmovies1[0:300]                  #mantuve solo la cantidad de lineas con las que quiero trabajar
    jetmovies.to_csv("jetmovies.csv", sep=";", index=False)


def interfaz():                                    #con esta funcion se hace la "presentacion" del proyecto
    centro = 140                                   #estas variables son para centrar el texto en la terminal, para que valgan la pena...
    sangria = 70                                   #... la terminal tiene que estar en pantalla completa.

    print("")
    print("")
    print(("Bienvenido a Jetmovies").center(centro))
    print(("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨").center(centro))
    print("")
    print("")
    print(("¡Tenemos una seleccion de mas de 101 peliculas!").center(centro))   
    print("")
    print("")
    print("")
    print("")
    print(("¿Quiere... ").center(40))
    print("")
    print("")
    print(("A: ver todas las películas?").center(62))#esta es, actualmente, la unica opcion que funciona hasta el final en el codigo
    print(("o").center(sangria))
    print(("B: seleccionar según clasificasión?").center(sangria))# esta es la otra opcion, la cual se puede hacer funcionar de manera muy similar a la...
    print("")                                                     #... opcion A, pero no alcance a hacerlo porque no me dio el tiempo para corregir...
    print("")                                                     #... los errores de las subopciones de B, paso que es necesario para hacer que ellas...
    seleccion = str(input(("               Selección: "))).capitalize() 
                                                                  #... funcionen tan bien como A.
    return seleccion
    
def interfaz2(seleccion, archivo):                 # esta funcion es la "interfaz 2", la cual depende de si se ha escogido la opcion A o la B
    
    centro = 140
    sangria = 70
    clear_screen()                                 # se limpia pantalla
    print("")
    print("")
    print("")
    print(("Jetmovies").center(centro))            # empieza con un titulo centrado del proyecto
    print("")
    print("")
    print("")
    
    if seleccion == "A":                           # si se ha escogido la opcion A...
        
        print("")
        peliculas = []
        for index, row in archivo.iterrows():      # se toman los nombres de las peliculas en una lista (para no hacer trabajar con objetos al pc todavia)
            pelicula = row["title"]
            peliculas.append(pelicula)              
        i = 0
        while (i < len(peliculas)):
            variable1 = peliculas[i]
            variable2 = peliculas[i+1]             # y se listan en pantalla por columnas concatenadas
            indice1 = i
            indice2 = i+1
            print("%-1s %-60s %-1s %s".center(30) %(indice1, variable1, indice2, variable2)) #aqui se concatenan los datos con un su indice correspondiente
            i = i+2                                # el paso del indice fue hecho porque la idea original era listar las peliculas en orden alfabetico...
                                                   #... y luego buscarlas en el archivo.csv para crear los objetos y sus atributos, pero...
        print("")                                  #... solo quedaron en el orden en que vienen en el archivo, aunque si se pueden buscar por su indice, lo que es genial.
        
        opcion = int(input("Seleccione un índice para ver la información de la película que desee: ").center(sangria)) #aqui se ingresa el indice correspondiente a la pelicula.

        nombre_pelicula = peliculas[opcion]
        #print(nombre_pelicula)
        
        return nombre_pelicula                     # de haber escogido la opcion A, se retorna el nombre de la pelicula a la que corresponde el indice seleccionado.     
        
    elif seleccion == "B":
        
        print("")
        print("")
        print("")
        print(("  Seleccione una clasificación para ver las películas relacionadas: ").center(sangria))
        print("")
        print("")
        print("")
        print("     A: Año de estreno.")
        print("     B: País de producción.")
        #print("     C: Calificación.")
        print("     C: Director.")
        print("     D: Idioma original.")
        #print("     F: Duración.")
        print("     E: Género.")
        print("")
        print("")
        
        opcion = str(input("     Su opción aquí: "))
        opcion = opcion.capitalize()
        
        return opcion                              # de haber escogido B, se retorna la subopcion que se haya elegido.
        
def crea_seleccion(archivo, opcion):               # esta funcion viene siendo una tercera interfaz,
    
    print("")
    clear_screen()
    
    if len(opcion) > 1:                            # la cual reconoce si el usuario escogio una pelicula de la opcion A
                                                   # y crea el objeto pelicula junto con todos sus atributos
        peliculas = []
        
        for index, row in archivo.iterrows():
            
            nombre_pelicula = row["title"]
            
            if opcion == nombre_pelicula:          # compara la pelicula seleccionada por el usuario con las peliculas del archivo.csv
                                                   # es por esto que si la opcion A ordenara las peliculas alfabeticamente el codigo funcionaria de la misma manera.
                anio = row["year"]
                duracion = row["duration"]
                pais = row["country"]              # toma todos los datos que pueden ser identificados facilmente (sin necesidad de separar por comas)...
                idioma = row["language"]           
                calificacion = row["avg_vote"]
                titulo = row["title"]
                
                pelicula = Pelicula()              
                pelicula.set_nombre(titulo.capitalize())
                pelicula.set_anio(anio)
                pelicula.set_duracion(duracion)    #... y los "settea" como atributos en el nuevo objeto "pelicula"
                pelicula.set_pais(pais)
                pelicula.set_idioma(idioma)
                pelicula.set_calificacion(calificacion)
                
                                                        # desde esta linea hasta la linea 163 lo que hace el codigo es separar los datos que si necesitan...
                generos = row["genre"]                  #... ser identificados por sus comas y los incluye como atributos del objeto "pelicula"
                actores = row["actors"]
                directores = row["director"]
                
                for genero in generos.split(", "):
                    pelicula.set_generos(genero)
                    
                for nombre in actores.split(", "):      # para el caso de los actores y los directores, crea los objetos y los ingresa en los array correspondientes...
                    actor = Actor()                     #... que vienen siendo los atributos "directores" y "actores" que trabajaron en la pelicula.
                    actor.set_nombre(nombre.capitalize())
                    pelicula.set_actores(actor)
                
                for nombre in directores.split(", "):
                    director = Director()
                    director.set_nombre(nombre.capitalize())
                    pelicula.set_directores(director)
                
                peliculas.append(pelicula)
                
        if len(peliculas) == 1:
                                        # aqui se imprime la informacion de la pelicula.
            for i in peliculas:
                
                print("La pelicula que usted selecciono es '{}' y su informacion es la siguiente: \n".format(i.get_nombre()))
                print("")
                print("El año en que se estreno fue el {}, Dura {} minutos.".format(i.get_anio(), i.get_duracion()))
                print("")
                print("Su Pais de origen es {}, fue calificada por los criticos con un {} de 10, su Idioma Original es {}".format(i.get_pais(), i.get_calificacion(), i.get_idioma()))
                print("")
                print("tiene el/los siguientes generos: ")
                for j in i.get_generos():
                    print(j)
                print("En ella trabajaron los siguientes actores: ")
                for j in i.get_actores():
                    print(j.get_nombre())
                print("... y el/los siguientes directores: ")
                for j in i.get_directores():
                    print(j.get_nombre())
                
         
        else:
            print("Opcion invalida, intentelo nuevamente.")
                                        #desde esta linea hasta la 339 corresponen a las subopciones sin terminar de la opcion B.
    elif opcion == "A":
        
        anios = []
        year = archivo["year"]
        
        for anio in year:
            encontrado = False
            if not anios:                #en todas se hace lo mismo, pero de distinta forma dependiendo de si es necesario:
                anios.append(anio)       
            else:
                for i in anios:          # se listan los atributos unicos de las peliculas (los paises, los años de produccion, los idiomas, etc)
                    if i == anio:        # y se imprimen en pantalla de forma concatenada para luego seleccionarlos de la misma forma que la opcion A.
                        encontrado = True
                        break
                if not encontrado:
                    anios.append(anio)
                    
        i = 0
        while (i < len(anios)):
            variable1 = anios[i]
            verificacion = i+2
            if verificacion <= len(anios):
                variable2 = anios[i+1]
            indice1 = i
            indice2 = i+1
            print("%-1s %-60s %-1s %s".center(30) %(indice1, variable1, indice2, variable2)) # aqui se imprimen concatenadamente.
            i = i+2
            
                    
    elif opcion == "B":
        
        paises = []
        country = archivo["country"]
            
        for pais in country:
            encontrado = False
            if not paises:
                paises.append(pais)         # se repite en todas las opciones, por eso no las comento todas.
            else:
                for i in paises:
                    if i == pais:
                        encontrado = True
                        break
                if not encontrado:
                    paises.append(pais)
                    
        i = 0
        while (i < len(paises)):
            variable1 = paises[i]
            verificacion = i+2
            if verificacion <= len(paises):
                variable2 = paises[i+1]
            indice1 = i
            indice2 = i+1
            print("%-1s %-60s %-1s %s".center(30) %(indice1, variable1, indice2, variable2))
            i = i+2
                    
    elif opcion == "C":
        
        directores = []
        
        for index, row in archivo.iterrows():
            directors = row["director"]
            directors = directors.split(", ")
    
            for director in directors:
                encontrado = False
                if not directores:
                    directores.append(director)
                else:
                    for i in directores:
                        if i == director:
                            encontrado = True
                            break
                    if not encontrado:
                        directores.append(director)
        
        i = 0
        while (i < len(directores)):
            variable1 = directores[i]
            verificacion = i+2
            if verificacion <= len(directores):
                variable2 = directores[i+1]
            indice1 = i
            indice2 = i+1
            print("%-1s %-60s %-1s %s".center(30) %(indice1, variable1, indice2, variable2))
            i = i+2
        
                
    elif opcion == "D":
        
        idiomas = []
        language = archivo["language"]
        
        for idioma in language:
            encontrado = False
            if not idiomas:
                idiomas.append(idioma)
            else:
                for i in idiomas:
                    if i == idioma:
                        encontrado = True
                        break
                if not encontrado:
                    idiomas.append(idioma)
        
        i = 0
        while (i < len(idiomas)):
            variable1 = idiomas[i]
            verificacion = i+2
            if verificacion <= len(idiomas):
                variable2 = idiomas[i+1]
            indice1 = i
            indice2 = i+1
            print("%-1s %-60s %-1s %s".center(30) %(indice1, variable1, indice2, variable2))
            i = i+2
        
    elif opcion == "E":
        
        generos = []
        
        for index, row in archivo.iterrows():
            genre = row["genre"]
            genre = genre.split(", ")
            
            for genero in genre:
                encontrado = False
                if not generos:
                    generos.append(genero)
                else:
                    for i in generos:
                        if i == genero:
                            encontrado = True
                            break
                    if not encontrado:
                        generos.append(genero)
            
        i = 0
        while (i < len(generos)):
            variable1 = generos[i]
            verificacion = i+2
            if verificacion <= len(generos):
                variable2 = generos[i+1]
            indice1 = i
            indice2 = i+1
            print("%-1s %-60s %-1s %s".center(30) %(indice1, variable1, indice2, variable2))
            i = i+2
                    
    else:
        print("Opcion invalida, intente nuevamente.")
        
        
    
        
        
        
    
        
def seleccion_A(archivo):           # las seleccion_A y seleccion_B son funciones que iban a ser usadas en un primer intento por crear opciones A y B...
    centro = 140
    sangria = 70
    print("")
    print("")
    print(("Seleccione un índice para ver la información de la película que desee:").center(sangria))
    print("")
    print("")
    
    lista_peliculas = archivo["title"]
    
    #for i in lista_peliculas:
        
        #variable1 = lista_peliculas[i]
        #variable2 = lista_peliculas[i+1]
        #variable3 = lista_peliculas[i+2]
        #print("%-20s, %-20s, %s" %(variable1, variable2, variable3))
        #i = i+3
    
    print(lista_peliculas)
    lista_peliculas.sort()
    print(lista_peliculas)
    indice = int(input(("Su seleccion aqui: ").center(sangria)))
    
    
    nombre_pelicula = lista_peliculas[indice]
    print(nombre_pelicula)
    
    #for indice, row in data.iterrows():
        
    
    print("{}".format(lista_peliculas[indice]))
    
    return indice

def seleccion_B(archivo):           #... pero me parecio poco practico y me sirvieron para hacer muchas pruebas del codigo.
    print("")
    print("")
    print(("Seleccione una clasificación para ver las películas relacionadas: ").center(sangria))
    print("")
    print("")
    print("")
    print(("A: Año de estreno.").center(sangria))
    print(("B: País de producción.").center(sangria))
    print(("C: Calificación.").center(sangria))
    print(("D: Director.").center(sangria))
    print(("E: Idioma original.").center(sangria))
    print(("F: Duración.").center(sangria))
    print(("G: Género.").center(sangria))
    print("")
    print("")
    opcion = str(input(("Su opción aquí: ").center(sangria)))
    opcion = opcion.capitalize()
    print ("{}".format(opcion))        


def crea_peliculas(archivo):
                                    # esta funcion crea los objetos pelicula entregandoles todos sus atributos excepto directores y actores.
    peliculas = []                  # me sirvio para entender como hacer todo, pero con la idea de optimizar la memoria, me parecio que era mejor idea seleccionar opciones y crear un unico objeto pelicula a la vez (con sus objetos directores y actores)
    
    for index, row in archivo.iterrows():
        anio = row["year"]
        duracion = row["duration"]
        pais = row["country"]
        idioma = row["language"]
        calificacion = row["avg_vote"]
        titulo = row["title"]
        
        pelicula = Pelicula()
        pelicula.set_nombre(titulo.capitalize())
        pelicula.set_anio(anio)
        pelicula.set_duracion(duracion)
        pelicula.set_pais(pais)
        pelicula.set_idioma(idioma)
        pelicula.set_calificacion(calificacion)
        
        generos = row["genre"]
        for genero in generos.split(", "):
            pelicula.set_generos(genero)
        
        peliculas.append(pelicula)
    
#    for titulo in archivo["title"]:
 #       pelicula = Pelicula()
  #      pelicula.set_nombre(titulo.capitalize())
   #     peliculas.append(pelicula)
    
    return peliculas

def crea_directores(archivo):
    directores = []
                                            # esta funcion crea los objetos director de forma unica, funciona como el ejemplo de top50 del profesor.
    for nombre in archivo["director"]:
        encontrado = False
        if not directores:                
            director = Director()
            director.set_nombre(nombre.capitalize())
            directores.append(director)
        else:
            for j in directores:
                if j.get_nombre() == nombre.capitalize():
                    encontrado = True
                    break
            if not encontrado:
                director = Director()
                director.set_nombre(nombre.capitalize())
                directores.append(director)
    
    return directores
    
def crea_actores(archivo):
    actores = []
                                            # esta funcion hace lo mismo que la anterior, pero con actores.
    for nombre in archivo["actors"]:
        encontrado = False
        if not actores:                
            actor = Actor()
            actor.set_nombre(nombre.capitalize())
            actores.append(actor)
        else:
            for j in actores:
                if j.get_nombre() == nombre.capitalize():
                    encontrado = True
                    break
            if not encontrado:
                actor = Actor()
                actor.set_nombre(nombre.capitalize())
                actores.append(actor)
    
    return actores
    

def crea_personas(archivo, profesion):      # esta funcion iba a crear directores y actores segun fuera necesario, pero como no utilice herencia me complique un tanto pensando en como hacerla funcionar.
                                            # me rendi con ella porque me parecio mas importante avanzar con el proyecto.
    profesionales = []
    
    for nombre in archivo["profesion"]:
        encontrado = False
        if not profesionales:                
            pelicula = Pelicula()
            pelicula.set_nombre(i.capitalize())
            peliculas.append(pelicula)
        else:
            for j in peliculas:
                if j.get_nombre() == i.capitalize():
                    encontrado = True
                    break
            if not encontrado:
                pelicula = Pelicula()
                pelicula.set_nombre(i.capitalize())
                peliculas.append(pelicula)
    
    return profesionales
    
        
def prueba(archivo):                         # esta funcion me ayudo a probar como separar los datos por comas.
    for titulo_pelicula in archivo["title"]:
        print(titulo_pelicula, ": \n")
        directores = row["director"]
        for nombre_director in directores.split(", "):
            print(nombre_director, ", ")
        


if __name__ == "__main__":
    
    data = procesa_archivo()
    #filtra_columnas(data)
        
    seleccion = interfaz()
    opcion = interfaz2(seleccion, data)  
    crea_seleccion(data, opcion)
    
                                # exceptuando este comentario, el resto me sirvieron para ir probando la generalidad del codigo y ver como hacer funcionar todo junto
    
    #if (seleccion == "A"):
        #seleccion_A(data)
    #elif(seleccion == "B"):
        #seleccion_B(data)
        
    #peliculas = crea_peliculas(data)
    #for i in peliculas:
    #    print("{}, {}, {}, {}, {}, {}.".format(i.get_nombre(), i.get_duracion(), i.get_idioma(), i.get_calificacion(), i.get_pais(), i.get_generos()))
        
        
    #crea_objetos(data)
    #prueba(data)
    
    
