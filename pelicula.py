#!/usr/bin/env python3
# -*- coding:utf-8 -*-



class Pelicula():

    def __init__(self):
        self.__nombre = None
        self.__generos = []
        self.__pais = None
        self.__año = 0
        self.__duracion = 0
        self.__idioma = None
        self.__calificacion = 0
        self.__actores = []
        self.__directores = []
        

    def get_nombre(self):
        return self.__nombre
    def set_nombre(self, nombre):
        if isinstance(nombre, str):
            self.__nombre = nombre
    
    def get_generos(self):
        return self.__generos
    def set_generos(self, genero):
        if isinstance(genero, str):
            self.__generos.append(genero)
            
    def get_pais(self):
        return self.__pais
    def set_pais(self, pais):
        if isinstance(pais, str):
            self.__pais = pais
            
    def get_anio(self):
        return self.__anio
    def set_anio(self, anio):
        if isinstance(anio, int):
            self.__anio = anio
            
    def get_duracion(self):
        return self.__duracion
    def set_duracion(self, duracion):
        if isinstance(duracion, int):
            self.__duracion = duracion
    
    def get_idioma(self):
        return self.__idioma
    def set_idioma(self, idioma):
        if isinstance(idioma, str):
            self.__idioma = idioma
            
    def get_calificacion(self):
        return self.__calificacion
    def set_calificacion(self, calificacion):
        if isinstance(calificacion, float):
            self.__calificacion = calificacion    
            
    def get_actores(self):
        return self.__actores
    def set_actores(self, actor):
        self.__actores.append(actor)
            
    def get_directores(self):
        return self.__directores
    def set_directores(self, director):
        self.__directores.append(director)
